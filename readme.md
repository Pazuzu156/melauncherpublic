# MELauncher Public Repository
How did you get here? The default landing page is the [MELauncher Wiki](https://bitbucket.org/Pazuzu156/melauncherpublic/wiki)

Nontheless, you're here, so I should say something. That's correct...right?  
Right, well, here.

Welcome to the Public MELauncher Repository. There won't be much to see here, so check out the [Wiki](https://bitbucket.org/Pazuzu156/melauncherpublic/wiki), the [Issue Tracker](https://bitbucket.org/Pazuzu156/melauncherpublic/issues) or maybe the [Downloads](https://bitbucket.org/Pazuzu156/melauncherpublic/downloads) are what you are looking for.
